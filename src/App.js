import "./App.css";
import Home from "./components/myntra/Screens/Home";
import Header from "./components/general/Header";
import Cart from "./components/context/CartContext";
import { BrowserRouter, Route } from "react-router-dom";

function App() {
    return (
        <div>
            <BrowserRouter>
                <Route>
                    <Cart>
                        <Header />
                        <Home />
                    </Cart>
                </Route>
            </BrowserRouter>
        </div>
    );
}

export default App;
