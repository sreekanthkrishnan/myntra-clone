import React, { useContext, useState } from "react";
import styled from "styled-components";
import { CartContext } from "../context/CartContext";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import PermIdentityIcon from "@material-ui/icons/PermIdentity";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import LocalMallIcon from "@material-ui/icons/LocalMall";

export default function Header() {
    const { counter } = useContext(CartContext);
    const [menuType, setMenuType] = useState("");
    const [is_dropDown, setDropDown] = useState(false);
    const [menu, setMenu] = useState([
        { type: "Men", url: "#" },
        { type: "Women", url: "#" },
        { type: "Kids", url: "#" },
        { type: "Home & living", url: "#" },
        { type: "Offers", url: "#" },
    ]);
    return (
        <Container>
            <LogoSection to="#">
                <Logo src={require("../../assets/logo/app-logo.png").default} />
            </LogoSection>
            <MenuBar>
                {menu.map((data) => (
                    <Menu
                        menuType={menuType}
                        to={data.url}
                        onMouseEnter={() => (setDropDown(true), setMenuType(data.type))}
                        // onMouseLeave={() => setDropDown(false)}
                    >
                        {data.type}
                    </Menu>
                ))}
            </MenuBar>
            <SearchBar>
                <SearchIcon style={{ color: "#9092a1", fontSize: "24px" }} />
                <Search placeholder="Search for products, brands and more" />
            </SearchBar>
            <UserMenu>
                <MenuCover>
                    <PermIdentityIcon style={{ color: "#94969f" }} />
                    <Label>Profile</Label>
                </MenuCover>
                <MenuCover>
                    <FavoriteBorderIcon style={{ color: "#94969f" }} />
                    <Label>Wishlist</Label>
                </MenuCover>
                <MenuCover>
                    <LocalMallIcon style={{ color: "#94969f" }} />
                    <Label>Wishlist</Label>
                    <CountSpan>0</CountSpan>
                </MenuCover>
            </UserMenu>
            <DropDown
                className={is_dropDown ? "active" : ""}
                // onMouseEnter={() => setDropDown(true)}
                onMouseLeave={() => setDropDown(false)}
            ></DropDown>
        </Container>
    );
}

const Container = styled.div`
    position: fixed;
    top: 0;
    width: 100%;
    padding: 0 50px;
    z-index: 99;
    /* height: 80px; */
    display: flex;
    justify-content: space-between;
    align-items: center;
    background-color: #fff;
    box-shadow: 0 4px 12px 0 rgb(0 0 0 / 5%);
`;
const LogoSection = styled(Link)`
    width: 50px;
`;
const Logo = styled.img`
    width: 100%;
`;
const MenuBar = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;
const Menu = styled(Link)`
    /* margin-right: 40px; */
    font-family: "whitney-semibold";
    text-transform: uppercase;
    color: #2a2c3f;
    font-size: 14px;
    padding: 0 20px;
    display: flex;
    align-items: center;
    height: 76px;
    border-bottom: 4px solid #fff;
    &:last-child {
        margin-right: 0;
    }
    &:hover {
        border-bottom: 4px solid
            ${(props) =>
                props.menuType === "Men"
                    ? "red"
                    : props.menuType === "Women"
                    ? "#fb56c1"
                    : props.menuType === "Kids"
                    ? "#f26a10"
                    : props.menuType === "Home & living"
                    ? "#f2c210"
                    : props.menuType === "Offers"
                    ? "#0db7af"
                    : null};
    }
`;
const SearchBar = styled.div`
    background-color: #f5f5f6;
    min-width: 450px;
    height: 45px;
    border-radius: 5px;
    padding: 0 0 0 20px;
    display: flex;
    align-items: center;
`;
const Search = styled.input`
    flex: 1;
    color: #9092a1;
    margin-left: 10px;
    &::placeholder {
        font-size: 14px;
    }
`;
const UserMenu = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;
const MenuCover = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-right: 25px;
    position: relative;
    cursor: pointer;
    &:last-child {
        margin-right: 0px;
    }
`;
const Label = styled.p`
    text-align: center;
    width: 100%;
    font-size: 12px;
    font-family: "whitney-semibold";
    color: #2a2c3f;
    line-height: 16px;
`;
const CountSpan = styled.span`
    position: absolute;
    width: 15px;
    height: 15px;
    right: 0px;
    top: -3px;
    border-radius: 50%;
    background-color: #ef4c4c;
    font-size: 12px;
    color: #fff;
    font-family: "whitney-bold";
    display: flex;
    justify-content: center;
    align-items: center;
`;
const DropDown = styled.div`
    position: absolute;
    min-height: 70vh;
    box-shadow: 0 16px 24px rgb(0 0 0 / 10%);
    width: 70%;
    margin: 0 auto;
    background-color: #fff;
    top: 0;
    margin-top: 90px;
    z-index: 9;
    transform: translate(11%, 10px);
    transition: 0.3s;
    opacity: 0;
    display: none;
    &.active {
        display: block !important;
        opacity: 1 !important;
    }
`;
