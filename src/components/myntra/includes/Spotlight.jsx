import React, { useEffect, useState } from "react";
import styled from "styled-components";
import axios from "axios";

export default function Spotlight() {
    const [banner, setBanner] = useState([]);
    const [isLoading, setLoading] = useState(true);

    useEffect(() => {
        FetchData();
    }, []);

    const FetchData = () => {
        axios
            .get("https://api.npoint.io/a76dd3d9caaf75a2deeb")
            .then((response) => {
                const { statusCode, data } = response.data;
                console.log(statusCode, data);
                if (statusCode === 6000) {
                    setBanner(data);
                    setLoading(false);
                } else if (statusCode === 6001) {
                    setLoading(false);
                }
            })
            .catch((error) => {
                setLoading(false);
            });
    };
    const [current, setCurrent] = useState(0);

    return (
        <Container>
            <BannerCover>
                {banner.map((data, index) => (
                    <>
                        {data.id - 1 === current && (
                            <BannerContainer
                                className={data.id - 1 === current ? "active" : null}
                                key={index}
                            >
                                <BannerImage src={data.image} alt="spotlight" />
                            </BannerContainer>
                        )}
                    </>
                ))}
            </BannerCover>
            <SliderDots>
                {banner.map((data, index) => (
                    <>
                        <Dots
                            className={index === current ? "active" : null}
                            key={index}
                            onClick={() => setCurrent(index)}
                        ></Dots>
                    </>
                ))}
            </SliderDots>
        </Container>
    );
}
const Container = styled.div`
    color: #000;
`;
const BannerCover = styled.div`
    display: flex;
    align-items: center;
    .active {
        transform: translateX(0px);
    }
`;
const BannerContainer = styled.div`
    min-width: 100%;
    transition: all 2s;
    transform: translateX(3000px);
`;
const BannerImage = styled.img`
    width: 100%;

    transition: all 2s;
`;
const SliderDots = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 10px;
    .active {
        background-color: #7e818c !important;
    }
`;
const Dots = styled.span`
    width: 8px;
    height: 8px;
    border-radius: 50%;
    display: block;
    background-color: #dfe0e2;
    margin-right: 12px;
    &:last-child {
        margin-right: 0;
    }
`;
