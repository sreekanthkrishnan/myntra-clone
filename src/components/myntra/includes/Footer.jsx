import React, { useState } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import YouTubeIcon from "@material-ui/icons/YouTube";
import InstagramIcon from "@material-ui/icons/Instagram";

export default function Footer() {
    const [menu, setMenu] = useState([
        { id: 1, type: "", menu: "Men" },
        { id: 2, type: "", menu: "Women" },
        { id: 3, type: "new", menu: "Kids" },
        { id: 4, type: "", menu: "Home & Living" },
        { id: 5, type: "", menu: "Offers" },
        { id: 6, type: "", menu: "Gift Cards" },
        { id: 7, type: "new", menu: "Myntra Insider" },
    ]);
    const [useFulLink, setUseFullLink] = useState([
        { id: 1, type: "", menu: "Contact us" },
        { id: 2, type: "", menu: "FAQ" },
        { id: 3, type: "", menu: "T & C" },
        { id: 4, type: "", menu: "Terms of Use" },
        { id: 5, type: "", menu: "Track Order" },
        { id: 6, type: "", menu: "Shipping" },
        { id: 7, type: "", menu: "Cancellation" },
        { id: 8, type: "", menu: "Returns" },
        { id: 9, type: "", menu: "Whitehat" },
        { id: 10, type: "", menu: "Blog" },
        { id: 11, type: "", menu: "Careers" },
        { id: 12, type: "", menu: "Privacy policy" },
        { id: 13, type: "", menu: "Site map" },
    ]);
    const [stamp, setStamp] = useState([
        {
            id: 1,
            type: "",
            description: "guarantee for all products at myntra.com",
            bold: "100% ORIGINAL ",
            image: require("../../../assets/Images/extra/orginal.png").default,
        },
        {
            id: 2,
            type: "",
            description: "of receiving your order",
            bold: "Return within 30days ",
            image: require("../../../assets/Images/extra/return.png").default,
        },

        {
            id: 3,
            type: "",
            description: "for every order above Rs. 799",
            bold: "Get free delivery",
            image: require("../../../assets/Images/extra/delivery.png").default,
        },
    ]);

    return (
        <Container>
            <Wrapper>
                <TopSection>
                    <MenuSection>
                        <Title>online shopping</Title>
                        {menu.map((data) => (
                            <MenuCover>
                                <Menu key={data.id} to="#">
                                    {data.menu}
                                </Menu>
                                {data.type === "new" ? <Label>new</Label> : null}
                            </MenuCover>
                        ))}
                    </MenuSection>
                    <UseFulLink>
                        <Title>Useful links</Title>
                        {useFulLink.map((data) => (
                            <MenuCover>
                                <Menu key={data.id} to="#">
                                    {data.menu}
                                </Menu>
                                {/* {data.type === "new" ? <Label>new</Label> : null} */}
                            </MenuCover>
                        ))}
                    </UseFulLink>
                    <AppSection>
                        <Title>EXPERIENCE MYNTRA APP ON MOBILE</Title>
                        <AppContainer>
                            <AppLink>
                                <App src="https://constant.myntassets.com/web/assets/img/bc5e11ad-0250-420a-ac71-115a57ca35d51539674178941-apple_store.png" />
                            </AppLink>
                            <AppLink>
                                <App src="https://constant.myntassets.com/web/assets/img/80cc455a-92d2-4b5c-a038-7da0d92af33f1539674178924-google_play.png" />
                            </AppLink>
                        </AppContainer>
                        <Title>Keep in touch</Title>
                        <IconDraw>
                            <FacebookIcon
                                style={{ color: "#696e79", fontSize: 38, marginRight: 10 }}
                            />
                            <TwitterIcon
                                style={{ color: "#696e79", fontSize: 38, marginRight: 10 }}
                            />
                            <YouTubeIcon
                                style={{ color: "#696e79", fontSize: 38, marginRight: 10 }}
                            />
                            <InstagramIcon
                                style={{ color: "#696e79", fontSize: 38, marginRight: 10 }}
                            />
                        </IconDraw>
                    </AppSection>
                    <LabelContainer>
                        {stamp.map((data) => (
                            <LabelCard>
                                <Stamp>
                                    <App src={data.image} />
                                </Stamp>
                                <Content>
                                    <span
                                        style={{
                                            color: "#000",
                                            fontSize: "16px",
                                            fontFamily: "whitney-semibold",
                                        }}
                                    >
                                        {data.bold}
                                    </span>
                                    {data.description}
                                </Content>
                            </LabelCard>
                        ))}
                    </LabelContainer>
                </TopSection>
                <PopularSearch>
                    <PopularTitle>SHOP ONLINE AT MYNTRA WITH COMPLETE CONVENIENCE</PopularTitle>
                    <Searches>
                        Another reason why Myntra is the best of all online stores is the complete
                        convenience that it offers. You can view your favourite brands with price
                        options for different products in one place. A user-friendly interface will
                        guide you through your selection process. Comprehensive size charts, product
                        information and high-resolution images help you make the best buying
                        decisions. You also have the freedom to choose your payment options, be it
                        card or cash-on-delivery. The 30-day returns policy gives you more power as
                        a buyer. Additionally, the try-and-buy option for select products takes
                        customer-friendliness to the next level. <br />
                        <br />
                        Enjoy the hassle-free experience as you shop comfortably from your home or
                        your workplace. You can also shop for your friends, family and loved-ones
                        and avail our gift services for special occasions.
                    </Searches>
                    <br />
                    <PopularTitle>HISTORY OF MYNTRA</PopularTitle>
                    <Searches>
                        Becoming India’s no. 1 fashion destination is not an easy feat. Sincere
                        efforts, digital enhancements and a team of dedicated personnel with an
                        equally loyal customer base have made Myntra the online platform that it is
                        today. The original B2B venture for personalized gifts was conceived in 2007
                        but transitioned into a full-fledged ecommerce giant within a span of just a
                        few years. By 2012, Myntra had introduced 350 Indian and international
                        brands to its platform, and this has only grown in number each passing year.
                        Today Myntra sits on top of the online fashion game with an astounding
                        social media following, a loyalty program dedicated to its customers, and
                        tempting, hard-to-say-no-to deals.
                        <br />
                        The Myntra shopping app came into existence in the year 2015 to further
                        encourage customers’ shopping sprees. Download the app on your Android or
                        IOS device this very minute to experience fashion like never before
                    </Searches>
                </PopularSearch>
            </Wrapper>
        </Container>
    );
}
const Container = styled.div`
    background-color: #fafbfc;
    padding: 50px;
`;
const Wrapper = styled.div`
    width: 80%;
    margin: 0 auto;
`;
const TopSection = styled.div`
    display: grid;
    grid-template-columns: 2fr 1fr 4fr 4fr;
    grid-column-gap: 20px;
`;
const Title = styled.h3`
    color: #535766;
    font-size: 13px;
    font-family: "whitney-semibold";
    margin-bottom: 20px;
    text-transform: uppercase;
`;
const MenuSection = styled.div``;
const Menu = styled(Link)`
    font-size: 16px;
    color: #9698a2;
    margin-bottom: 5px;
`;
const Label = styled.span`
    background-color: #ed3833;
    color: #fff;
    font-size: 12px;
    width: 40px;
    height: 16px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 6px;
    font-family: "whitney-semibold";
    margin-left: 5px;
`;
const MenuCover = styled.div`
    display: flex;
    align-items: center;
`;
const UseFulLink = styled.div``;
const AppSection = styled.div``;
const AppContainer = styled.div`
    display: flex;
    padding-right: 50px;
    margin-bottom: 25px;
`;
const AppLink = styled.div`
    margin-right: 10px;
`;

const App = styled.img`
    width: 100%;
`;
const IconDraw = styled.div`
    display: flex;
    /* align-items: top; */
`;
const LabelContainer = styled.div``;
const LabelCard = styled.div`
    display: flex;
    margin-bottom: 20px;
`;
const Stamp = styled.div`
    width: 40px;
    margin-right: 10px;
`;
const Content = styled.p`
    max-width: 205px;
`;

const PopularSearch = styled.div`
    margin-top: 30px;
`;
const PopularTitle = styled.h3`
    font-size: 13px;
    font-family: "whitney-semibold";
    margin-bottom: 20px;
    text-transform: uppercase;
    display: flex;
    align-items: center;
    color: #535766;
`;
const Searches = styled.p`
    font-size: 14px;
`;
