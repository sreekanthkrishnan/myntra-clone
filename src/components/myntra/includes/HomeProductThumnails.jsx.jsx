import React, { useState } from "react";
import styled from "styled-components";

export default function HomeProductThumnails({ title, data }) {
    return (
        <Container>
            <TItle>{title}</TItle>
            <DealsContainer>
                {data.map((data) => (
                    <Deals>
                        <DealBanner src={data.banner} />
                    </Deals>
                ))}
            </DealsContainer>
        </Container>
    );
}

const Container = styled.div``;
const TItle = styled.h3`
    text-transform: uppercase;
    color: #3f4152;
    font-size: 28px;
    font-family: "whitney-semibold";
    letter-spacing: 4px;
    margin: 40px 0px 10px 30px;
`;
const DealsContainer = styled.div`
    padding: 40px 60px;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    grid-column-gap: 12px;
    grid-row-gap: 15px;
`;
const Deals = styled.div``;
const DealBanner = styled.img`
    width: 100%;
`;
