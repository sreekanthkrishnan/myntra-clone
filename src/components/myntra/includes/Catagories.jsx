import React, { useState } from "react";
import styled from "styled-components";

export default function Catagories({ title, data }) {
    return (
        <Container>
            <Title>{title}</Title>

            <CatagoriesCover>
                {data.map((data) => (
                    <Thumnail key={data.id}>
                        <ThumnailImage src={data.thumnail} alt={data.name} />
                    </Thumnail>
                ))}
            </CatagoriesCover>
        </Container>
    );
}
const Container = styled.div``;
const Title = styled.h3`
    text-transform: uppercase;
    color: #3f4152;
    font-size: 28px;
    font-family: "whitney-semibold";
    letter-spacing: 4px;
    margin: 40px 0px 10px 30px;
`;
const CatagoriesCover = styled.div`
    display: grid;
    padding: 50px 60px;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
    grid-column-gap: 20px;
    grid-row-gap: 15px;
`;
const Thumnail = styled.div``;
const ThumnailImage = styled.img`
    width: 100%;
`;
