import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Catagories from "../includes/Catagories";
import HomeProductThumnails from "../includes/HomeProductThumnails.jsx";
import Spotlight from "../includes/Spotlight";
import axios from "axios";
import Footer from "../includes/Footer";

export default function Home() {
    const [isLoading, setLoading] = useState(true);
    const [isError, setError] = useState(true);
    const [errorMessages, setErrorMessage] = useState("*some error occur please try again");
    const [deals, setDeals] = useState([]);

    const [brandedDeals, setBrandedDeals] = useState([]);
    const [catagories, setCategory] = useState([]);

    // Api data fetching
    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = () => {
        axios
            .get("https://api.npoint.io/56652d3d86fcfdf30b42")
            .then((response) => {
                const { statusCode, data } = response.data;
                // console.log(statusCode, data);
                if (statusCode === 6000) {
                    setCategory(data);
                    setLoading(false);
                } else if (statusCode === 6001) {
                    setLoading(false);
                    setError(true);
                }
            })
            .catch((error) => {
                setLoading(false);
                setError(true);
                setErrorMessage(error);
            });

        axios
            .get("https://api.npoint.io/b0826b8ee13e410cc5f2")
            .then((response) => {
                const { statusCode, data } = response.data;
                // console.log(statusCode, data);
                if (statusCode === 6000) {
                    setBrandedDeals(data);
                    setLoading(false);
                } else if (statusCode === 6001) {
                    setLoading(false);
                }
            })
            .catch((error) => {
                setLoading(false);
                setError(true);
                setErrorMessage(error);
            });

        axios
            .get("https://api.npoint.io/623e3a79da2d1aa68438")
            .then((response) => {
                const { statusCode, data } = response.data;
                // console.log(statusCode, data);
                if (statusCode === 6000) {
                    setDeals(data);
                    setLoading(false);
                } else if (statusCode === 6001) {
                    setLoading(false);
                }
            })
            .catch((error) => {
                setLoading(false);
                setError(true);
                setErrorMessage(error);
            });
    };
    return (
        <Container>
            <Spotlight />
            <Catagories title={"Top catogories"} data={catagories} />
            <HomeProductThumnails title={"Deals of day"} data={deals} />
            <HomeProductThumnails title={"BIGGEST DEALS ON TOP BRANDS"} data={brandedDeals} />
            <Footer />
        </Container>
    );
}
const Container = styled.div`
    margin-top: 120px;
`;
