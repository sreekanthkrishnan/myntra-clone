import React, { createContext, useState } from "react";

export const CartContext = createContext();

const Cart = ({ children }) => {
    const [counter, setCounter] = useState(0);

    const addToCart = () => {
        setCounter((count) => count + 1);
    };

    return (
        <CartContext.Provider value={{ counter, addToCart }}>
            {children}
        </CartContext.Provider>
    );
};

export default Cart;
